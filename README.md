# simple-pong

The `token` is a trigger token: 

- go to the CI/CD settings of the project
- expand the **Pipeline triggers** section
- create a token

```shell
curl -X POST \
-F token="5f53ab2274c93b51b60b1597837939" \
-F ref="master" \
-F variables[REMOTE_MESSAGE]="Hello people" \
-F variables[REMOTE_FROM]="terminal" \
http://gitlab-faas.eu.ngrok.io/api/v4/projects/experiments%2fpong/trigger/pipeline

```